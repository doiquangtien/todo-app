const initValue ={
    filter:{
        searchText:"",
        searchStatus:"All",
    },
    todoList : JSON.parse(localStorage.getItem('todos')) || []
}

const rootReducer=(state = initValue, action)=> {
    switch(action.type){
        case "ADD_TODO" :
            let newTodoList = [...state.todoList, action.payload];
            localStorage.setItem("todos", JSON.stringify(newTodoList));
        return {
            ...state,
            todoList: newTodoList
        }
        case "DEL_TODO":
            const newDelTodo = [...state.todoList]
            newDelTodo.splice(action.payload,1)
            localStorage.setItem("todos", JSON.stringify(newDelTodo));
            return{
                ...state,
                todoList: newDelTodo
            }
        case "UPDATE_TODO":
            const newUpdateTodos = [...state.todoList]
            const updatedTodo = newUpdateTodos.map(
                (newUpdateTodo,index) => {
                  if (action.index === index) {
                    return { ...newUpdateTodo, name: action.payload.name, priority: action.payload.priority };
                  }
                  return newUpdateTodo;
                }
              );
            localStorage.setItem("todos", JSON.stringify(updatedTodo));
            return{
                ...state,
                todoList : updatedTodo
            }
        case "UPDATE_CHECK_TODO":
            const newUpdateCheckTodos = [...state.todoList]
            const updateCheckTodo =  newUpdateCheckTodos.map((newUpdateCheckTodo,index)=>{
                if(action.index === index){
                    return {...newUpdateCheckTodo, completed: action.payload}
                }
                return newUpdateCheckTodo
            })
            localStorage.setItem("todos", JSON.stringify(updateCheckTodo));
            return{
                ...state,
                todoList : updateCheckTodo
            }
        case "CLEAR_ALL_TODO":
            localStorage.removeItem("todos");
            return{
                ...state,
                todoList: action.payload
            }
        case "SEARCH_TODO":
            return{
                ...state,
                filter:{...state.filter,searchText: action.payload }
            }
        case "FILTER_BY_STATUS":
            return{
                ...state,
                filter:{...state.filter,searchStatus:action.payload}
            }
        default:
            return state
    }
}

export default rootReducer