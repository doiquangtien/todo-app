export const todoListSelector = (state) => {
    return state.todoList.filter((todo)=>{
        if(state.filter.searchStatus === "All"){
            return todo.name.includes(state.filter.searchText)
        }
        return todo.name.includes(state.filter.searchText) && (state.filter.searchStatus === "Completed" ? todo.completed : !todo.completed)
    })
}
// export const filterSearchSelector =(state)=> state.filter.searchText
// export const filterStatusSelector =(state)=> state.filter.searchStatus
