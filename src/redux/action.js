export const addTodoList = (data)=>{
    return {
        type : "ADD_TODO",
        payload : data
    }
}
export const delTodo = (index)=>{
    return {
        type : "DEL_TODO",
        payload: index
    }
}
export const updateTodo = (text,index)=>{
    return {
        type : "UPDATE_TODO",
        payload: text,
        index: index
    }
}

export const updateCheckTodo = (boolean,index)=>{
    return {
        type : "UPDATE_CHECK_TODO",
        payload: boolean,
        index: index
    }
}

export const clearAllTodo = (data)=>{
    return {
        type : "CLEAR_ALL_TODO",
        payload : data
    }
}

export const searchTodo = (text)=>{
    return {
        type : "SEARCH_TODO",
        payload: text,
    }
}

export const filterByStatus = (text)=>{
    return {
        type : "FILTER_BY_STATUS",
        payload: text,
    }
}
