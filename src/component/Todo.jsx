import "./todo.css";
import { useDispatch } from "react-redux";
import { updateCheckTodo } from "../redux/action";
import { useState } from "react";
function Todo({
  data,
  index,
  editActive,
  setEditActive,
  handleUpdatedData,
  handleDel,
}) {
  const dispatch = useDispatch();
  const [inputEdit, setInputEdit] = useState(data.name);
  const [priori, setPriori] = useState(data.priority);
  const isEdit = editActive === index;
  const handleSelect = (e) => {
    setPriori(e.target.value);
  };
  const handleCheck = () => {
    dispatch(updateCheckTodo(!data.completed, index));
  };
  const handleInputEdit = (e) => {
    setInputEdit(e.target.value);
  };
  const onBlurEdit = () => {
    setEditActive(null);
    handleUpdatedData(inputEdit, priori, index);
  };
  return (
    <div className="wrap-todo">
      <div className="left">
        <input
          className="todo-check"
          checked={data.completed}
          onChange={handleCheck}
          type="checkbox"
        ></input>
      </div>
      <div
        className="right"
        onDoubleClick={() => {
          setEditActive(index);
        }}
      >
        {isEdit ? (
          <div className="wrap-edit">
            <input
              className="input-edit"
              type="text"
              value={inputEdit}
              onChange={handleInputEdit}
            />

            <select
              className="select-prio-edit"
              value={priori}
              onChange={handleSelect}
            >
              <option value="High">High</option>
              <option value="Medium">Medium</option>
              <option value="Low">Low</option>
            </select>
            <button className="btn" onClick={onBlurEdit}>
              Update
            </button>
          </div>
        ) : (
          <>
            <span
              className="todo-name"
              style={{
                ...(data.completed === true
                  ? { opacity: 0.5, textDecoration: "line-through" }
                  : {}),
              }}
            >
              {data.name}
            </span>
            <div
              className="todo-priority"
              style={{
                ...(data.completed === true
                  ? { opacity: 0.5, textDecoration: "line-through" }
                  : {}),
              }}
            >
              {data.priority}
              <button
                className="btn-del"
                onClick={() => {
                  handleDel(index);
                }}
              >
                X
              </button>
            </div>
          </>
        )}
      </div>
    </div>
  );
}
export default Todo;
