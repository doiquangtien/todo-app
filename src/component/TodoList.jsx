import React, { useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Todo from "./Todo";
import "./todoList.css";
import {
  addTodoList,
  delTodo,
  updateTodo,
  clearAllTodo,
} from "../redux/action";
import { todoListSelector } from "../redux/selector";
import FilterTodo from "./FilterTodo";

function TodoList() {
  const dispatch = useDispatch();
  const todoList = useSelector(todoListSelector);
  // const searchText = useSelector(filterSearchSelector);
  // const searchStatus = useSelector(filterStatusSelector);
  const [input, setInput] = useState("");
  const [priori, setPriori] = useState("High");
  const [editActive, setEditActive] = useState(null);
  const inputRef = useRef();
  const handleSelect = (e) => {
    setPriori(e.target.value);
  };
  const handleInput = (e) => {
    setInput(e.target.value);
  };

  const handleAdd = () => {
    setInput("");
    inputRef.current.focus();
    setPriori("High");
    dispatch(
      addTodoList({
        name: input,
        completed: false,
        priority: priori,
      })
    );
  };
  const handleDel = (index) => {
    dispatch(delTodo(index));
  };
  const handleUpdatedData = (input, priori, indexTodo) => {
    dispatch(updateTodo({ name: input, priority: priori }, indexTodo));
  };
  return (
    <div className="Wrap-todoList">
      <div className="todo-app">
        <h1 className="title">Todo-App</h1>
        <FilterTodo />
        <div className="bottom">
          <input
            className="input-todo"
            ref={inputRef}
            type="text"
            value={input}
            onChange={handleInput}
          />
          <select
            className="select-prio"
            value={priori}
            onChange={handleSelect}
          >
            <option value="High">High</option>
            <option value="Medium">Medium</option>
            <option value="Low">Low</option>
          </select>

          <button
            className="btn"
            onClick={() => {
              input.length > 0 && handleAdd();
            }}
          >
            Add
          </button>
        </div>
        <span style={{ marginRight: "20px" }}>Total : {todoList.length}</span>
        <button
          className="btn"
          onClick={() => {
            dispatch(clearAllTodo([]));
          }}
        >
          Clear all
        </button>
        <div className="top">
          {todoList.map((data, index) => (
            <div key={index}>
              <Todo
                data={data}
                index={index}
                editActive={editActive}
                setEditActive={setEditActive}
                handleUpdatedData={handleUpdatedData}
                handleDel={handleDel}
              />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default TodoList;
