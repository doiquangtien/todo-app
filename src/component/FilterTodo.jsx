import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { searchTodo, filterByStatus } from "../redux/action";
import "./filter.css";

function FilterTodo() {
  const dispatch = useDispatch();
  const [searchInput, setSearchInput] = useState("");
  const [searchStatus, setSearchStatus] = useState("All");

  const handleSearchInput = (e) => {
    setSearchInput(e.target.value);
    dispatch(searchTodo(e.target.value));
  };
  const handleSearchStatus = (e) => {
    setSearchStatus(e.target.value);
    dispatch(filterByStatus(e.target.value));
  };
  return (
    <div className="wrap-filter">
      <div className="title-filter">Search</div>
      <input
        className="input-filter"
        type="text"
        value={searchInput}
        onChange={handleSearchInput}
      />
      <div className="title-filter">Filter by status</div>
      <div
        className="radio-group"
        value={searchStatus}
        onChange={handleSearchStatus}
      >
        <input type="radio" id="html" name="fav_language" value="All" />
        <label htmlFor="html">All</label>
        <input type="radio" id="html" name="fav_language" value="Completed" />
        <label htmlFor="html">Completed</label>
        <input type="radio" id="html" name="fav_language" value="Todo" />
        <label htmlFor="html">Todo</label>
      </div>
    </div>
  );
}

export default FilterTodo;
